# Explorer la planification dans k8s

------------

 
><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


## Leçon sur la Planification Kubernetes

Dans cette leçon, nous allons explorer la planification dans Kubernetes. Voici un aperçu rapide de ce que nous allons aborder dans cette leçon. Nous commencerons par répondre à la question : qu'est-ce que la planification ? Nous discuterons du processus de planification. Nous parlerons de l'utilisation de `nodeSelector` et de `nodeName`, puis nous ferons une brève démonstration pratique.

### Qu'est-ce que la Planification ?
La planification est simplement le processus d'attribution de pods aux nœuds Kubernetes afin que les kubelets puissent les exécuter. Chaque fois que nous créons un pod, quelque chose doit déterminer sur quel nœud exécuter ce pod, et c'est le processus de planification.

Le scheduler est un composant particulier du plan de contrôle qui gère et exécute ce processus de planification. Chaque fois que nous créons un pod, le scheduler détermine où ce pod doit s'exécuter.

### Processus de Planification
Le scheduler Kubernetes sélectionne un nœud approprié pour chaque pod. Voici quelques éléments à prendre en compte que le scheduler examine pendant ce processus :
- **Demandes de ressources versus ressources disponibles des nœuds** : Le scheduler examine les demandes de ressources et évite de planifier un pod sur des nœuds qui n'ont pas suffisamment de ressources pour répondre à ces demandes.
- **Étiquettes des nœuds (node labels)** : Nous pouvons personnaliser le processus de planification pour sélectionner ou refuser certains nœuds en fonction de divers facteurs, généralement les étiquettes des nœuds.

### Utilisation de `nodeSelector`
Le `nodeSelector` est assez simple dans son fonctionnement. Nous pouvons configurer un `nodeSelector` sur nos pods et il limitera les nœuds sur lesquels le pod peut être planifié. Les `nodeSelectors` utilisent des étiquettes pour filtrer les nœuds appropriés.

```yaml
apiVersion: V1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx
  nodeSelector:
    mylabel: myvalue
```

### Utilisation de `nodeName`
`nodeName` permet de contourner entièrement la planification et d'assigner simplement un pod à un nœud spécifique en utilisant le nom de ce nœud.

```yaml
apiVersion: V1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx
  nodeName: k8s-worker1
```


## Démonstration Pratique

Connectez-vous à votre nœud de contrôle Kubernetes et exécutez les commandes suivantes :

1. Listez les nœuds disponibles :

```sh
kubectl get nodes
```

2. Ajoutez une étiquette à l'un de vos nœuds de travail :

```sh
kubectl label nodes <node_name> special=true
```

3. Créez un pod avec `nodeSelector` :

```bash
nano nodeselector-pod.yml
```

Contenu : 

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nodeselector-pod
spec:
  containers:
  - name: nginx
    image: nginx
  nodeSelector:
    special: "true"
```

```sh
kubectl apply -f nodeselector-pod.yml
```

4. Vérifiez l'état du pod et sur quel nœud il est planifié :

```sh
kubectl get pods -o wide
```

5. Créez un pod avec `nodeName` :

```bash
nano nodename-pod.yml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nodename-pod
spec:
  containers:
  - name: nginx
    image: nginx
  nodeName: <node_name>
```

```sh
kubectl apply -f nodename-pod.yml
```

6. Vérifiez l'état du pod et sur quel nœud il est planifié :

```sh
kubectl get pods -o wide
```

## Résumé
Dans cette leçon, nous avons répondu à la question : qu'est-ce que la planification ? Nous avons fait un aperçu du processus de planification et des éléments que le scheduler prend en compte. Nous avons parlé de l'utilisation de `nodeSelector` et de `nodeName`, et nous avons fait une brève démonstration pratique. C'est tout pour cette leçon. À la prochaine !


# Reférences

https://kubernetes.io/docs/concepts/scheduling-eviction/kube-scheduler/

https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/